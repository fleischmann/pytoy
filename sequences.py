import math
import helpers

def fibonacci(n):
    """return frist n elements of the fibonacci sequence with seeds 0 and 1"""
    res = [0, 1]
    for i in range(n-3):
        res.append(res[-1] + res[-2])
    return res


def naturalsquares(n):
    return [x**2 for x in range(n)]


def primes(n):
    """return first n primes"""
    nn = n**2
    mask = []
    for i in range(nn):
        mask.append(True)
    for i in range(2, int(math.floor(math.sqrt(nn)))):
        if mask[i]:
            j = i**2
            while j < nn:
                mask[j] = False
                j += i
    primes = []
    for i in range(nn):
        if mask[i]:
            primes.append(i)
    return primes[0:n]

def mersenne(n):
    """return first n mersenne prime exponents"""
    res = [2]
    i = 3
    while len(res) < n:
        if helpers.llt(i):
            res.append(i)
        i += 1
    return res
