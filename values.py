def fibonacci(n):
    if n==0 or n==1:
        return n
    return fibonacci(n-1) + fibonacci(n-2)

def pi(n):
    """return approximation of pi by Leibniz' series with n elements"""
    j = 1
    pi = 0
    for i in range(1,n+1):
        pi -= (-1)**(i) * 4/j
        j += 2
    return pii

def factorial(n):
    if n==0:
        return 0
    return n*factorial(n-1)
